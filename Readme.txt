Đồ án nhóm G2
Nhóm 00

*IDE: visual studio 2017

*Các chức năng đã cài đặt:
- Tạo giao diện ứng dụng giống hình minh họa. Các button được disabled/enabled theo yêu cầu đề.
- Minimize: thu nhỏ ứng dụng xuống system tray. User có thể click mouse phải lên
icon của ứng dụng ở system tray để mở lại dialog UI.
- Exit: gỡ bỏ hook và đóng ứng dụng.
- Start: cài đặt hook.
- Stop: hủy bỏ hook, button Stop sẽ disabled và button Start sẽ enabled.
- Hook: bao gồm 2 hook là mouse hook và keyboard hook:
	. Mouse hook: khi có left button up thì gửi lệnh Ctrl + C cho hệ thống (điểm hạn chế nhóm chưa khắc phục được)
	. Keyboard hook: khi người dùng nhấn Ctrl + Shift thì sẽ mở clipboard ra lấy dữ liệu về, rồi gán đó vào text cho chương trình
	. Lấy được các từ được select từ hầu hết các ứng dụng
	. Khi lấy từ xong có xử lý sắp xếp theo thứ tự alphabet để hiện thị Lookup words

*Cách build project:
- Build project HookDLL trước tiên.
- Sau đó build project BT-G-00.
