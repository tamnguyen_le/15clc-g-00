﻿// HookDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#define EXPORT _declspec(dllexport)
#define WM_GET_TEXT_CLIPBOARD (WM_APP + 0x0001) //new message, send to main app to get clipboard data

//Added by Khoi - 1553018 - 15/01/2017

//global variables
HHOOK hHookKeyBoard = NULL;
HHOOK hHookMouse = NULL;
HINSTANCE hInstLib;
HWND hMain; //main app handle
HWND hfocus; //focus window handle

 //Added by Khoi - 1553018 - 15/07/2017
bool sendCtrlC() {
	int key_count = 4; //(keys down = 2) + (keys up = 2) = 4
	bool return_val = true;

	INPUT* input = new INPUT[key_count]; //sequence of actions of keys (Ctrl,c)
	for (int i = 0; i < key_count; i++)
	{
		input[i].ki.dwFlags = 0;
		input[i].type = INPUT_KEYBOARD;
	}

	// Send Ctrl + c keys (key down)
	input[0].ki.wVk = VK_CONTROL;
	input[0].ki.wScan = MapVirtualKey(VK_CONTROL, MAPVK_VK_TO_VSC);
	input[1].ki.wVk = 0x43; // Virtual key code for 'c'
	input[1].ki.wScan = MapVirtualKey(0x43, MAPVK_VK_TO_VSC);
	
	// Key up
	input[2].ki.dwFlags = KEYEVENTF_KEYUP; //flag: key is up
	input[2].ki.wVk = input[0].ki.wVk;
	input[2].ki.wScan = input[0].ki.wScan;
	input[3].ki.dwFlags = KEYEVENTF_KEYUP;//flag: key is up
	input[3].ki.wVk = input[1].ki.wVk;
	input[3].ki.wScan = input[1].ki.wScan;

	//send key combination to current active window
	if (SendInput(key_count, (LPINPUT)input, sizeof(INPUT)) == 0)
		return_val = false;

	delete[] input;
	return return_val;
}

//Added by Khoi - 1553018 - 21/07/2017
bool sendCtrlShiftUp() {
	int key_count = 2; // (keys up = 2)
	bool return_val = true;

	INPUT* input = new INPUT[key_count]; //sequence of actions of keys (Ctrl,c)
	for (int i = 0; i < key_count; i++)
	{
		input[i].ki.dwFlags = 0;
		input[i].type = INPUT_KEYBOARD;
	}

	// Send Ctrl + Shift keys (key up)
	input[0].ki.wVk = VK_CONTROL;
	input[0].ki.wScan = MapVirtualKey(VK_CONTROL, MAPVK_VK_TO_VSC);
	input[0].ki.dwFlags = KEYEVENTF_KEYUP;
	input[1].ki.wVk = VK_SHIFT; // Virtual key code for 'c'
	input[1].ki.wScan = MapVirtualKey(VK_SHIFT, MAPVK_VK_TO_VSC);
	input[1].ki.dwFlags = KEYEVENTF_KEYUP;
	
	//send key combination to current active window
	if (SendInput(key_count, (LPINPUT)input, sizeof(INPUT)) == 0)
		return_val = false;

	delete[] input;
	return return_val;
}

//Added by Khoi - 1553018 - 15/07/2017
LRESULT CALLBACK keyboardProc(int nCode, WPARAM wParam, LPARAM lParam) {
	if (nCode < 0) // không xử lý message 
		return CallNextHookEx(hHookKeyBoard, nCode, wParam, lParam);
	if (nCode >= 0) {

		if (GetAsyncKeyState(VK_CONTROL) && GetAsyncKeyState(VK_SHIFT)) { //user use: Ctrl + Shift


			/*SetForegroundWindow(hMain);*/
			//send defined message to main app
			
			//send ctrl + shift up
			//Added by Khoi - 1553018 - 21/07/2017
			sendCtrlShiftUp();
			
			SendMessage(hMain, WM_GET_TEXT_CLIPBOARD, wParam, lParam);
			
		}
		return CallNextHookEx(hHookKeyBoard, nCode, wParam, lParam);
	}
}

//Added by Khoi - 1553018 - 15/07/2017
LRESULT CALLBACK mouseProc(int nCode, WPARAM wParam, LPARAM lParam) {
	if (nCode < 0) // không xử lý message 
		return CallNextHookEx(hHookKeyBoard, nCode, wParam, lParam);
	if (nCode >= 0) {
		if (wParam == WM_LBUTTONUP) {

			//get handle of focused window
			HWND curWnd = GetForegroundWindow();
			DWORD activeThreadID = 0, processID;
			activeThreadID = GetWindowThreadProcessId(curWnd, &processID);
			DWORD curThreadID = GetCurrentThreadId();

			if (activeThreadID != curThreadID)
				AttachThreadInput(activeThreadID, curThreadID, true);

			hfocus = GetFocus();

			//send keys: ctrl + c
			sendCtrlC();

		}
		return CallNextHookEx(hHookKeyBoard, nCode, wParam, lParam);
	}
}

//Added by Khoi - 1553018 - 15/07/2017
extern "C" EXPORT void _RemoveHook(HWND hWnd) {
	if (hHookKeyBoard == NULL && hHookMouse == NULL)	 return;
	UnhookWindowsHookEx(hHookKeyBoard);
	UnhookWindowsHookEx(hHookMouse);
	hHookKeyBoard = NULL;
	hHookMouse = NULL;
	MessageBox(hWnd, L"Remove Hook complete", L"NOTICE", MB_OK);
}

//Added by Khoi - 1553018 - 15/07/2017
extern "C" EXPORT void _InstallHook(HWND hWnd) {

	if (hHookKeyBoard != NULL && hHookMouse != NULL) return;
	hHookKeyBoard = SetWindowsHookEx(WH_KEYBOARD, (HOOKPROC)keyboardProc, hInstLib, 0);
	hHookMouse = SetWindowsHookEx(WH_MOUSE, (HOOKPROC)mouseProc, hInstLib, 0);
	DWORD a = GetLastError();
	hMain = hWnd;
	if (hHookKeyBoard && hHookMouse) //success
	{
		MessageBox(hWnd, L"Setup Hook Complete", L"NOTICE", MB_OK);
	}
	else MessageBox(hWnd, L"Setup Hook fail", L"NOTICE", MB_OK);
}
