// BT-G-00.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "Shellapi.h"
#include "BT-G-00.h"
#include <CommCtrl.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include "Dictionary.h"
#pragma comment(lib, "comctl32.lib");

/////////////
//Dialog interface is added by Nhan, on 11/7/2017
////////////
#define MAX_LOADSTRING 100
// Purpose: receiving message when keyboard hook succeeds
#define WM_GET_TEXT_CLIPBOARD (WM_APP + 0x0001)		// Added by Tam, on 15/7/2017, Fixed by Khoi - 21/07/2017
#define WM_TRAY_RIGHT_CLICK	   WM_USER+2        // Added by Khoa, on 18/7/2017
#define WM_CREATE_DIALOG (WM_USER + 0x0003)				// Added by Tam, on 20/7/2017

// Added by Tam, on 15/7/2017
// Data structure to store data of the word user selected
struct QuickDictData {

	WCHAR text[256]; //modified by Khoi - 21/07/2017
	WCHAR time[18];
	WCHAR isViewed[5];//modified by Khoi - 21/07/2017

	QuickDictData();
	~QuickDictData();

	void getTime();
};

///////////////////////
// Global Variables:


HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
//WCHAR *szText;									// Buffer for retrieving data from clipboard	// Added by Tam, on 15/7/2017
std::vector<QuickDictData> wordList;			// List of words that user selected				// Added by Tam, on 15/7/2017
HWND hWndListView;								// handler of list view
static int selitm = -1;							//	determine selectec row						//	Added by Dat, on 20/07/2017
Dictionary dictionary;
///////////////////////


///////////////////////
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

// Added by Tam, on 15/7/2017
INT_PTR CALLBACK	DlgWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
void InstallHook(HWND hWnd);
void RemoveHook(HWND hWnd);
bool order_by_alphabet(QuickDictData& i, QuickDictData& j);
///////////////////////

// Added by Dat, on 20/7/2017
HWND CreateListView(HWND hwndParent);	// Function to create List-view of words
BOOL InitListViewColumns(HWND hWndListView); //	Adds columns to a list - view control
void DisplayItem(HWND hWndListView);	//Display item in word list
LVITEM SelectWord();					//	Select item
void DeleteWord();						// Delete selected word

// Added by Tam, on 12/8/2017
int searchWord();

struct petinfo
{
	TCHAR szKind[10];
	TCHAR szBreed[50];
	TCHAR szPrice[20];
};

petinfo pi[] =
{
	{ TEXT("Dog"), TEXT("Poodle"), TEXT("$300.00") },
	{ TEXT("Cat"), TEXT("Siamese"), TEXT("$100.00") },
	{ TEXT("Fish"), TEXT("Angel Fish"), TEXT("$10.00") }
};

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_BTG00, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_BTG00));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_BTG00));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_BTG00);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable
	/////////////////////////////////////////
	//Fix show icon in taskbar, dont show window, change by Nhan 23/7/2017
	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		0, 0, 0, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	//ShowWindow(hWnd, SW_MINIMIZE);
	UpdateWindow(hWnd);
	/////////////////////////////////////////
	return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_CREATE:
		{
			dictionary.loadDictionaryFromFile("dictionary.txt");
			PostMessage(hWnd, WM_CREATE_DIALOG, wParam, lParam);
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
		case WM_CREATE_DIALOG:
		{
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgWndProc);
		}
		break;
		case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_ABOUT:
					DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
					break;
				case IDM_EXIT:
					DestroyWindow(hWnd);
					break;
				default:
					return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
		break;
		/*case WM_ACTIVATE:
			ShowWindow(hWnd, SW_HIDE);
			break;*/
		// Added by Tam, on 15/7/2017
		case WM_GET_TEXT_CLIPBOARD: {

			//MessageBox(hWnd, L"A", L"B", MB_OK);
			OutputDebugString(L"CustomMessage");
			// Open clipboard for retrieving data
			if (OpenClipboard(NULL)) {

				// Copy data from clipboard
				HGLOBAL hglb = GetClipboardData(CF_UNICODETEXT);
				WCHAR*  lpstr = (WCHAR*)GlobalLock(hglb);
				WCHAR szText[256];
				wsprintf(szText, lpstr);

				// Check if word already existed in the list
				for (auto&& word : wordList) 
					if (wcscmp(szText, word.text) == 0)
						return DefWindowProc(hWnd, message, wParam, lParam);

				// Add word to the global word list
				wordList.push_back(QuickDictData());
				wsprintf(wordList.back().text,szText);
				wordList.back().getTime();

				// Sort word list after adding
				std::sort(wordList.begin(), wordList.end(), order_by_alphabet);

				GlobalUnlock(hglb);
				CloseClipboard();
			}
		}
		break;
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code that uses hdc here...
			EndPaint(hWnd, &ps);
		}
		break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
		case WM_INITDIALOG: {
		
			return (INT_PTR)TRUE;
		}

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
			{
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK DlgWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
		case WM_INITDIALOG:
		{
			//	Added by Dat, on20/07/2017
			// Create List View 
			CreateListView(hDlg);
			// Enable to select row of item
			SelectWord();
			return (INT_PTR)TRUE;
		} break;
		// Display word in the list
		case WM_ACTIVATE:
		{
			DisplayItem(hWndListView);
			// Added by Dat, on 21/07/2017
			// Enable & Disable buttons Delete All, Delete Word, Search Word
			if (wordList.empty())
			{
				EnableWindow(GetDlgItem(hDlg, IDC_DELETEALL), FALSE);
				EnableWindow(GetDlgItem(hDlg, IDC_DELETEONE), FALSE);
				EnableWindow(GetDlgItem(hDlg, IDC_SEARCH), FALSE);
			}
			else
			{
				EnableWindow(GetDlgItem(hDlg, IDC_DELETEALL), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_DELETEONE), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_SEARCH), TRUE);
			}
		}
		break;
	case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			// Parse the menu :
			switch (wmId)
			{
				// Added by Tam, on 15/7/2017
				case ID_START:
				{
					InstallHook(GetParent(hDlg));
					// Added by Dat, on 15/7/2017
					EnableWindow(GetDlgItem(hDlg, IDC_STOP), TRUE);
					EnableWindow(GetDlgItem(hDlg, ID_START), FALSE);
				} break;
				// Added by Tam, on 15/7/2017
				case IDC_STOP:
				{
					RemoveHook(GetParent(hDlg));
					// Added by Dat, on 15/7/2017
					EnableWindow(GetDlgItem(hDlg, IDC_STOP), FALSE);
					EnableWindow(GetDlgItem(hDlg, ID_START), TRUE);
				}	break;
				// Added by Dat, on 15/7/2017
				case IDC_MINIMIZE:
				{
					/* Function to minimize the app*/
					// Added by Khoa, on 18/7/2017
					//Hide the dialog
					ShowWindow(hDlg, SW_HIDE);

					// Show icon in system tray.

					NOTIFYICONDATA nid;
					nid.cbSize = sizeof(NOTIFYICONDATA);
					nid.hWnd = hDlg;
					nid.uID = 0;
					nid.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;

					nid.uCallbackMessage = WM_TRAY_RIGHT_CLICK;
					nid.hIcon = LoadIcon(0, IDI_APPLICATION);
					wcscpy_s(nid.szTip, 64, L"Right Click To Maximize.");
					Shell_NotifyIcon(NIM_ADD, &nid);
					
					/*SetWindowLong(hDlg, GWL_EXSTYLE, GetWindowLong(hDlg, GWL_EXSTYLE) | ~WS_EX_APPWINDOW);*/
				}
				break;
				// Added by Tam, on 12/8/2017
				case IDC_SEARCH:
				{
					int searchResult = searchWord();
					if (searchResult != -2) {
						dictionary.showWord(searchResult, GetParent(hDlg));

					}
				}

				// Added by Dat, on 20/7/2017
				// Function Delete Selected word
				case IDC_DELETEONE:
				{
					DeleteWord();
					// Added by Dat, on 21/07/2017
					// Enable & Disable buttons Delete All, Delete Word, Search Word
					if (wordList.empty())
					{
						EnableWindow(GetDlgItem(hDlg, IDC_DELETEALL), FALSE);
						EnableWindow(GetDlgItem(hDlg, IDC_DELETEONE), FALSE);
						EnableWindow(GetDlgItem(hDlg, IDC_SEARCH), FALSE);

					}
				} break;
				// Added by Dat, on 15/7/2017
				// Function Delete All
				case IDC_DELETEALL:
				{
					ListView_DeleteAllItems(hWndListView);
					// Added by Dat, on 21/07/2017
					// Enable & Disable buttons Delete All, Delete Word, Search Word
					EnableWindow(GetDlgItem(hDlg, IDC_DELETEALL), FALSE);
					EnableWindow(GetDlgItem(hDlg, IDC_DELETEONE), FALSE);
					EnableWindow(GetDlgItem(hDlg, IDC_SEARCH), FALSE);
					wordList.clear();	// clear list of words
				} break;
				case ID_EXIT:
				{
					RemoveHook(GetParent(hDlg));
					wordList.clear();
					EndDialog(hDlg, FALSE);
					PostQuitMessage(0);
				} break;
				default:
					return DefWindowProc(hDlg, message, wParam, lParam);
			}
		}
		break;
		// Added by Tam, on 15/7/2017
		case WM_CLOSE:
		{
			wordList.clear();
			RemoveHook(GetParent(hDlg));
			PostQuitMessage(0);
		}
		break;
		// Added by Khoa, on 18/7/2017
		// Restore the minimized dialog from system tray
		case WM_TRAY_RIGHT_CLICK:
		{
			if (lParam == WM_RBUTTONDOWN)
			{
				ShowWindow(hDlg, SW_SHOW);
				/*SetWindowLong(hDlg, GWL_EXSTYLE, GetWindowLong(hDlg, GWL_EXSTYLE) & WS_EX_APPWINDOW);*/
				ShowWindow(hDlg, SW_RESTORE);
				/*SetForegroundWindow(hDlg);
				Shell_NotifyIcon(NIM_DELETE, &nid);*/
			}
		}
		break;
	}
	return (INT_PTR)FALSE;
}

// Added by Tam, on 15/7/2017
// Purpose: install global hook
void InstallHook(HWND hWnd) {

	typedef void(*MYPROC)(HWND);
	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	hinstLib = LoadLibrary(L"HookDLL.dll");
	if (hinstLib != NULL)//success
	{
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_InstallHook");
		if (ProcAddr != NULL)
			ProcAddr(hWnd);
	}
}

// Added by Tam, on 15/7/2017
// Purpose: remove global hook
void RemoveHook(HWND hWnd) {
	typedef void(*MYPROC)(HWND);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	hinstLib = LoadLibrary(L"HookDLL.dll");
	if (hinstLib != NULL)//success
	{
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_RemoveHook");
		if (ProcAddr != NULL)
			ProcAddr(hWnd);
	}
}



//////////////////////////////
// Added by Tam, on 15/7/2017
QuickDictData::QuickDictData()  { wsprintf(isViewed, L"N"); }

QuickDictData::~QuickDictData()
{
}

// Purpose: get the local system time and turn it into a string for displaying later
void QuickDictData::getTime()
{
	SYSTEMTIME curTime;
	GetLocalTime(&curTime);
	// string format: hh/mm  dd/mm/yyyy
	wsprintf(time, L"%dh%d'  %d/%d/%d", curTime.wHour, curTime.wMinute, curTime.wDay, curTime.wMonth, curTime.wYear);
}

// Purpose: function for sorting with alphabetical order
bool order_by_alphabet(QuickDictData & i, QuickDictData & j)
{
	// Modified by Tam, on 21/7/2017
	// Change all words to upper case for comparison
	std::string tmp_i, tmp_j;

	for (int counter = 0; counter < 256 && i.text[counter] != '\0'; ++counter)
		tmp_i.push_back(toupper(i.text[counter]));

	for (int counter = 0; counter < 256 && j.text[counter] != '\0'; ++counter)
		tmp_j.push_back(toupper(j.text[counter]));

	// i <= j
	return tmp_i < tmp_j;
	//return (wcscmp(i.text, j.text) <= 0);
}
//////////////////////////////

////////////////////////////////
//	Added by Dat, on 20/07/2017
HWND CreateListView(HWND hwndParent)
{
	INITCOMMONCONTROLSEX icex;           // Structure for control initialization.
	icex.dwICC = ICC_LISTVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	DWORD styleEx_v = LVS_EX_FULLROWSELECT	| LVS_EX_DOUBLEBUFFER | LVS_EX_LABELTIP;
	DWORD style_v = WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_EDITLABELS | WS_BORDER | LVS_EX_GRIDLINES;

	// Create the list-view window in report view with label editing enabled.
	hWndListView = CreateWindow(WC_LISTVIEW, L"", style_v, 25, 40, 500, 200,
		hwndParent, (HMENU)IDM_LIST, hInst, NULL);

	ListView_SetExtendedListViewStyle(hWndListView, styleEx_v);

	InitListViewColumns(hWndListView);
	
	return (hWndListView);
}

BOOL InitListViewColumns(HWND hWndListView)
{
	WCHAR szText[256];     // Temporary buffer.
	LVCOLUMN lvc;
	int iCol;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	// Add the columns.
	for (iCol = 0; iCol < 3; iCol++)
	{
		lvc.iSubItem = iCol;
		switch (iCol)
		{
		case 0:
		{
			lvc.pszText = TEXT("Word");
			lvc.cx = 150;
		}	break;
		case 1:
		{	lvc.pszText = TEXT("Added Time");
		lvc.cx = 200;
		}	break;
		case 2:
		{
			lvc.pszText = TEXT("Viewed");
			lvc.cx = 150;
		}	break;
		}

		lvc.fmt = LVCFMT_CENTER;

									// Load the names of the column headings from the string resources.
		LoadString(hInst, IDS_FIRSTCOLUMN + iCol, szText, sizeof(szText) / sizeof(szText[0]));

		// Insert the columns into the list view.
		if (ListView_InsertColumn(hWndListView, iCol, &lvc) == -1)
			return FALSE;
	}

	return TRUE;
}

void DisplayItem(HWND hWndListView)
{
	ListView_DeleteAllItems(hWndListView);
	LVITEM lvi;
	memset(&lvi, 0, sizeof(LVITEM));
	lvi.mask = LVIF_TEXT | LVIF_PARAM | LVIF_STATE;
	lvi.state = 0;
	lvi.stateMask = 0;

	for (int k = 0; k < wordList.size(); k++)
	{
		lvi.iItem = k;
		lvi.iSubItem = 0;
		int res = ListView_InsertItem(hWndListView, &lvi);
		ListView_SetItemText(hWndListView, res, 0, wordList[k].text);
		ListView_SetItemText(hWndListView, res, 1, wordList[k].time);
		ListView_SetItemText(hWndListView, res, 2, wordList[k].isViewed); //Added by Khoi - 1553018 - 21/07/2017
	}
}

LVITEM SelectWord()
{
	DWORD dwProcessID;
	GetWindowThreadProcessId(hWndListView, &dwProcessID);
	HANDLE process = OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_QUERY_INFORMATION, FALSE, dwProcessID);

	LVITEM lvi;

	LVITEM* _lvi = (LVITEM*)VirtualAllocEx(process, NULL, sizeof(LVITEM), MEM_COMMIT, PAGE_READWRITE);

	lvi.state = LVIS_FOCUSED | LVIS_SELECTED;
	lvi.stateMask = LVIS_FOCUSED | LVIS_SELECTED;
	lvi.mask = LVIF_STATE;

	WriteProcessMemory(process, _lvi, &lvi, sizeof(LVITEM), NULL);
	SendMessage(hWndListView, LVM_SETITEMSTATE, (WPARAM)0, (LPARAM)_lvi);
	VirtualFreeEx(process, _lvi, 0, MEM_RELEASE);

	return lvi;
}

void DeleteWord()
{
	selitm = ListView_GetNextItem(hWndListView, -1, LVNI_SELECTED);
	if (selitm != -1)
	{
		ListView_DeleteItem(hWndListView, selitm);
		//delte element in vector
		wordList.erase(wordList.begin() + selitm);
	}
}

// Added by Tam on 12/8/2017
// Purpose: search for word in dictionary
//			return the index of the word in dictionary if found, -1 otherwise
//			return -2 if no item in list view is selected
int searchWord()
{
	int selectedItem = ListView_GetNextItem(hWndListView, -1, LVNI_SELECTED);

	if (selectedItem != -1) {
		wsprintf(wordList[selectedItem].isViewed, L"Y");
		return dictionary.searchDict(wordList[selectedItem].text);
	}

	return -2;
}
////////////////////////////////