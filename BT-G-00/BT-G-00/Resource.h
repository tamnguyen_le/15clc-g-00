//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BT-G-00.rc
//
#define IDC_MYICON                      2
#define IDD_BTG00_DIALOG                102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_BTG00                       107
#define IDI_SMALL                       108
#define IDC_BTG00                       109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDC_STOP                        1000
#define IDC_MINIMIZE                    1001
#define IDC_SEARCH                      1002
#define IDC_DELETEONE                   1003
#define IDC_DELETEALL                   1004
#define IDC_LIST_WORD                   1008
#define IDC_LIST_ADD                    1010
#define IDC_LIST_VIEW                   1011
#define ID_START                        1013
#define ID_EXIT                         1014
#define IDC_LIST1                       1015
#define IDC_STATIC                      -1
#define IDM_LIST						1030
#define IDS_FIRSTCOLUMN					1031
// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
