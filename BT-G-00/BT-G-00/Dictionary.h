#pragma once
#include<vector>
#include<sstream>
#include<fstream>
#include<locale>
#include<codecvt>
using namespace std;

//Added by Khoi - 1553018 - 11/08/2017
struct word {
	WCHAR viet[256];
	WCHAR anh[256];
};
class Dictionary
{
private:
	vector<word> dictionary;
public:
	void loadDictionaryFromFile(string fileName);
	int searchDict(WCHAR* word);

	void showWord(int index, HWND hWnd);
	//Dictionary();
};

