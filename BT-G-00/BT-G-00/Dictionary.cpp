#include "stdafx.h"
#include "Dictionary.h"


void Dictionary::loadDictionaryFromFile(string fileName)
{
	wifstream f(fileName);

	//define locale
	locale loc(locale(), new codecvt_utf8<wchar_t>);

	//set new faceset to f
	f.imbue(loc);

	if (f.fail()) return;

	while (!f.eof()) {
		wstring tempString;
		getline(f, tempString);

		int pos = tempString.find(':');

		//split string by  ": "
		wstring tempViet, tempAnh;
		//begin to pos of ":" -1
		tempAnh = tempString.substr(0, pos);
		//pos of ":" + 2 to end
		tempViet = tempString.substr(pos + 2);

		//convert string to WCHAR
		word tempWord;
		wsprintf(tempWord.anh, tempAnh.c_str());
		wsprintf(tempWord.viet, tempViet.c_str());

		dictionary.push_back(tempWord);
	}

	f.close();
}

// Added by Tam on 12/8/2017
int Dictionary::searchDict(WCHAR * word)
{
	std::string _word;

	// Convert input word to upper case
	for (int counter = 0; counter < 256 && word[counter] != '\0'; ++counter)
		_word.push_back(toupper(word[counter]));

	// Traverse dictionary
	for (int i = 1; i < dictionary.size(); ++i) {

		std::string tmp;
		// Convert words in dictionary to upper case
		for (int counter = 0; counter < 256 && dictionary[i].anh[counter] != '\0'; ++counter)
			tmp.push_back(toupper(dictionary[i].anh[counter]));

		if (tmp == _word)
			return i;
	}

	return -1;
}

// Added by Tam on 12/8/2017
void Dictionary::showWord(int index, HWND hWnd)
{
	if (index != -1) {

		WCHAR tmp[512];
		wcscpy(tmp, dictionary[index].anh);
		wcscat(tmp, L": ");
		wcscat(tmp, dictionary[index].viet);
		MessageBox(hWnd, tmp, L"Y nghia cua tu", MB_OK);
	}
	else
		MessageBox(hWnd, L"Tu khong ton tai trong tu dien nay.\nXin ban doi cap nhat phien ban sau tot hon.", L"Y nghia cua tu", MB_OK);

}
